import { LoginComponent } from './login.component';

export const LoginRoutes: Array<any> = [
  {
    path: '',
    component: LoginComponent,
    pathMatch: 'full'
  }
];
