import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';


export const APP_COMPONENTS: any[] = [
  AppComponent,
  AboutComponent,
  HomeComponent,
  DashboardComponent,
  LoginComponent
];

export * from './app.component';
export * from './about/about.component';
export * from './home/home.component';
export * from './dashboard/dashboard.component';
export * from './login/login.component';
